<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.toptal.com/resume/ratko-solaja
 * @since      1.0.0
 *
 * @package    Toptal_Save
 * @subpackage Toptal_Save/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Toptal_Save
 * @subpackage Toptal_Save/includes
 * @author     Ratko Solaja <ratko@toptal.com>
 */
class Toptal_Save_Activator {

	/**
	 * On activation create a page and remember it.
	 *
	 * Create a page named "Saved", add a shortcode that will show the saved items
	 * and remember page id in our database.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Saved Page Arguments
		$saved_page_args = array(
			'post_title'   => __( 'Saved', 'toptal-save' ),
			'post_content' => '[toptal-saved]',
			'post_status'  => 'publish',
			'post_type'    => 'page'
		);

		// Insert the page and get its id.
		$saved_page_id = wp_insert_post( $saved_page_args );

		// Save page id to the database.
		add_option( 'toptal_save_saved_page_id', $saved_page_id );

		/**
		 * Set unique cookie name.
		 */
		$site_url = get_bloginfo( 'url' );
		$site_name = get_bloginfo( 'name' );
		$suffix = '-toptal-saved-items';

		// Combine everything
		$cookie_name = $site_url . $site_name . $suffix;

		// Now let's strip everything
		$cookie_name = str_replace( array( '[\', \']' ), '', $cookie_name );
		$cookie_name = preg_replace( '/\[.*\]/U', '', $cookie_name );
		$cookie_name = preg_replace( '/&(amp;)?#?[a-z0-9]+;/i', '-', $cookie_name );
		$cookie_name = htmlentities( $cookie_name, ENT_COMPAT, 'utf-8' );
		$cookie_name = preg_replace( '/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $cookie_name );
		$cookie_name = preg_replace( array( '/[^a-z0-9]/i', '/[-]+/' ) , '-', $cookie_name );
		$cookie_name = strtolower( trim( $cookie_name, '-' ) );

		// Save the value to the database.
		add_option( 'toptal_save_unique_cookie_name', $cookie_name );

	}

}